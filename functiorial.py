

def factorial(ith):
    # ```returns ith factorial```
    return factorial(ith-1) * ith if ith > 0 else 1