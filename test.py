from functiorial import factorial

if __name__ == "__main__":
    if factorial(5) == 120:
        print("1 test je ok\n")
    else:
        raise ValueError('funkce factorial nepocita spravne 5!')

    if factorial(0) == 1:
        print("2 test je ok\n")
    else:
        raise ValueError('funkce factorial nepocita spravne 0!')

    if factorial(1) == 1:
        print("3 test je ok\n")
    else:
        raise ValueError('funkce factorial nepocita spravne 1!')